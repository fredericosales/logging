# Python3 and logging module

## Logging level

Logging é um meio de verificar o andamento de uma aplicação enquanto ela trabalha. Um evento é uma messagem
descritiva que pode conter informações sobre o andamento da aplicação ou até mesmo salvar os resultados da 
aplicação em um arquivo externo.

## Quando usar e o que usar

O modulo logging fornece funções para reportar o funcionamento de aplicações . Elas são: debug(), info(), warning(), 
error() e critical().

| Tarefa                          | melhor ferramenta                     |
|---------------------------------|---------------------------------------|
| mostrar saída no console        | print()                               |
| logging das saídas da aplicação | logging.debug() ou logging.info()     |
| erros em tempo de execução      | logging.warn() ou logging.warning()   |
| exceptions                     | logging.exception()                   |
| critical erros report           | logging.error() ou logging.critical() |

## Níveis de log

| Nível    | Quando Usar                                       |
|----------|---------------------------------------------------|
| DEBUG    | informação detalhada, para diagnósticos           |
| INFO     | confirmação de que as coisas estão como esperadas |
| WARNING  | indicação de que algo inesperado ocorreu          |
| ERROR    | para indicar que algo mais sério ocorreu          |
| CRITICAL | indica que algo realmente sério ocorreu           |

## Link para a documentação do logging

Link para a [documentação](https://docs.python.org/3/howto/logging.html). 
