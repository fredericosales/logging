#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Frederico Sales <frederico.sales@engenharia.ufjf.br>
2018
"""

# imports
import numpy as np
import logging
import matplotlib.pyplot as plt
import pdb
import datetime
import time

# warnings
def warn(*args, **kwargs):
    pass

import warnings
warnings.warn = warn

# =========================================================================== #
# debug

def debug(debug=False):
    if debug == True:
        pdb.set_trace()

# =========================================================================== #
# logging

ts = time.time()
DATA = datetime.date.today()
DATATIME = datetime.datetime.fromtimestamp(ts).strftime('%d-%m-%Y %H:%M:%S')
FORMAT = ''
LEVEL=logging.INFO
LOG_FILENAME = 'logfile_'+ str(DATA)  + '.log'
logging.basicConfig(filename=LOG_FILENAME, level=LEVEL, format=FORMAT)

# =========================================================================== #
# loggin

def logapp(name):
    """Aqui comeca o log."""
    logging.info(name + ' - ' + str(DATATIME))
    logging.info("# ================================================= #")
    logging.info("here comes nothing...\n")
    for x in range(50, 61):
        logging.info('{0:2d}\t{1:3d}\t{2:4d}\t {3:5}'.format(x, x*x, x*x*x, x*x+5))
    logging.info("# ================================================= #\n\n")

# =========================================================================== #

# =========================================================================== #

# =========================================================================== #

if __name__ == '__main__':
    """Here comes the pain."""
    logapp('dummy')
